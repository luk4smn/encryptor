/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.encryptor;

import com.brazil.form.MainJPanel;

/**
 *
 * @author lucas.nunes
 */
public class Main {
    public static void main(String[] args) 
    {
         try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {

                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        MainJPanel main =  new MainJPanel();
        main.setVisible(true);
    }
}
